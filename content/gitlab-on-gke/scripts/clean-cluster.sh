#!/bin/bash                                                                                             
# Written by
# v1 - eggshell - mctaylor@us.ibm.com                                     
# v2 - dgordon - dgordon@gitlab.com
#                                                                               
# This is intended to be used for cleaning up a kubernetes cluster after its       
# integration is removed from GitLab. This will allow you to reinstall software 
# via GitLab cleanly. Currently, it ensures Tiller, Ingress, Prometheus, and       
# Runner are cleaned up after.                                                  

if [ $# -lt 1 ]; then
   echo "Error: GitLab project name required."
   echo "Usage: $(basename $0) <GitLab project name>"
   exit 1
else
   APP="$1"
   CLUSTER="`kubectl config current-context | awk -F'_' '{print $NF}'`" 
   if [ -z "$CLUSTER" ]; then
      echo "Error: Cluster name is empty!"
      exit 1
   else
      echo "About to clear out cluster $CLUSTER."
      echo "Continue? (Y|n)"
      read ANSWER
      case "$ANSWER" in
      "Y"|"y")
         kubectl delete namespace gitlab-managed-apps                                    
         kubectl delete namespace $APP
         kubectl delete deploy tiller-deploy -n kube-system                              
         kubectl delete service tiller-deploy -n kube-system                             
                                                                                         
         kubectl delete clusterrole ingress-nginx-ingress                                
         kubectl delete clusterrole prometheus-kube-state-metrics                        
         kubectl delete clusterrole prometheus-prometheus-server                         
                                                                                         
         kubectl delete clusterrolebinding ingress-nginx-ingress                         
         kubectl delete clusterrolebinding tiller-admin                                  
         kubectl delete clusterrolebinding prometheus-kube-state-metrics                 
         kubectl delete clusterrolebinding prometheus-prometheus-server
         ;;
      *)
         echo "Ok, aborting."
         exit 0
      esac
   fi
fi
